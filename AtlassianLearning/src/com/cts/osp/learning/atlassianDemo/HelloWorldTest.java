package com.cts.osp.learning.atlassianDemo;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

public class HelloWorldTest extends TestCase{

	@Test
	public void testMain() {
		String output=HelloWorld.SayHello();
		assertEquals("Hello World", output);
		
	}

}
